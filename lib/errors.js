const responses = {
	INVALID_METHOD: {
		status: 405,
		message: "Invalid Method",
		success: false,
		errors: [
			{
				id: "INVAID_METHOD",
				message:
					"Request is using an invalid method, this route only accepts POST requests.",
				user_facing_message:
					"Request failed due to an invalid request method",
			},
		],
	},
	VALIDATION_FAILED: {
		status: 400,
		message: "Invalid Request",
		success: false,
		errors: [
			{
				id: "VALIDATION_FAILED",
				message: "Data sanitation/validation check has failed",
				user_facing_message: "Server failed to validate data",
			},
		],
	},
	BOX_NOT_FOUND: {
		status: 404,
		message: "Invalid Box ID",
		success: false,
		errors: [
			{
				id: "BOX_NOT_FOUND",
				message: "Could not locate box within database",
				user_facing_message: "Could not find the box",
			},
		],
	},
	USERNAME_TAKEN: {
		status: 400,
		message: "Username Taken",
		success: false,
		errors: [
			{
				id: "USERNAME_TAKEN",
				message: "Username is already in use",
				user_facing_message:
					"Username has been taken, please try a different username!",
			},
		],
	},
	USER_NOT_FOUND: {
		status: 404,
		message: "User Not Found",
		success: false,
		errors: [
			{
				id: "USER_NOT_FOUND",
				message: "Could not find account",
				user_facing_message:
					"Could not find account, did you enter the right username?",
			},
		],
	},
	INCORRECT_PASSWORD: {
		status: 403,
		message: "Incorrect Password",
		success: false,
		errors: [
			{
				id: "INCORRECT_PASSWORD",
				message: "Password does not match",
				user_facing_message: "You entered the wrong password",
			},
		],
	},
	INVALID_REGISTRATION_INPUT: {
		status: 400,
		message: "Invalid Username or Password",
		success: false,
		errors: [
			{
				id: "INVALID_REGISTRATION_INPUT",
				message:
					"An invalid or empty value for the username or password field was submitted",
				user_facing_message:
					"An invalid or empty value for the username or password field was submitted",
			},
		],
	},
	AUTHORIZATION_FAILED: {
		status: 401,
		message: "Unauthorized",
		success: false,
		errors: [
			{
				id: "AUTHORIZATION_FAILED",
				message: "Failed to verify user authentication",
				user_facing_message: "Failed to authorize user",
			},
		],
	},
	TOO_MANY_REQUESTS: {
		status: 429,
		message: "Too Many Requests",
		success: false,
		errors: [
			{
				id: "TOO_MANY_REQUESTS",
				message: "Too many requests have been sent",
				user_facing_message: "You have been ratelimitted",
			},
		],
	},
	INVALID_FILE_TYPE: {
		status: 400,
		message: "Invalid File Type",
		success: false,
		errors: [
			{
				id: "INVALID_FILE_TYPE",
				message: "Uploaded file is not a CSV file",
				user_facing_message:
					"Failed to upload file due to it not being a CSV file",
			},
		],
	},
	FILE_TOO_LARGE: {
		status: 413,
		message: "Payload Too Large",
		success: false,
		errors: [
			{
				id: "FILE_TOO_LARGE",
				message: "Uploaded file is too large to be uploaded (>16MB)",
				user_facing_message:
					"Failed to upload file due to it being too large",
			},
		],
	},
	INTERNAL_SERVER_ERROR: {
		status: 500,
		message: "Internal Server Error",
		success: false,
		errors: [
			{
				id: "INTERNAL_SERVER_ERROR",
				message: "An internal server error has occured",
				user_facing_message: "Request failed due to an unknown error",
			},
		],
	},
};

export default responses;
