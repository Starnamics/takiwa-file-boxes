import clientPromise from "./mongodb";
import crypto from "crypto";
import requestIp from "request-ip";

function getCurrentRequestBatchIdentifier() {
	const currentDate = new Date();
	const secondBatchIdentifier = Math.floor(currentDate.getSeconds() / 5);
	return `Y${currentDate.getFullYear()}M${currentDate.getMonth()}D${currentDate.getDate()}H${currentDate.getHours()}M${currentDate.getMinutes()}B${secondBatchIdentifier}`;
}

async function isRequestRatelimitted(request, maximumRequests) {
	const clientIp = requestIp.getClientIp(request);
	if (!clientIp) {
		return true;
	}
	const hashedIp = crypto.createHash("md5").update(clientIp).digest("hex");

	const batchIdentifier = getCurrentRequestBatchIdentifier();

	const client = await clientPromise;
	const db = client.db(process.env.MONGODB_DATABASE);
	const collection = db.collection("ratelimits");

	const date = new Date();
	date.setSeconds(date.getSeconds() + 5);

	const result = await collection.findOneAndUpdate(
		{
			ip: hashedIp,
			request_batch_identifier: batchIdentifier,
		},
		{
			$set: {
				ip: hashedIp,
				request_batch_identifier: batchIdentifier,
				expireAt: date,
			},
			$inc: { request_batch_amount: 1 },
		},
		{
			upsert: true,
			returnDocument: "after",
			willRetryWrite: true,
		}
	);

	if (result.value.request_batch_amount > maximumRequests) {
		return true;
	} else {
		return false;
	}
}

export default isRequestRatelimitted;
