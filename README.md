# Takiwa File Boxes Test

This web application was built as a test for Takiwa.

It was built using React.js, Node.js, Semantic UI and MongoDB Atlas Serverless.

## Setup

### Clone & Install

```batch
# clone repo
$ git clone https://gitlab.com/Starnamics/takiwa-file-boxes.git
# open src directory
$ cd ./takiwa-file-boxes
# install dependencies
$ npm install
```

### Start Development Server

```batch
# install Vercel CLI if you have not already
$ npm install -g vercel
# start development server
$ vercel dev
```

### Build & Deploy to Vercel

```batch
# setup & deploy vercel as a production build
$ vercel deploy --prod
```

### Enviroment Variables

While in development, you should use a .env file with the MongoDB URI in it.

```env
MONGODB_URI = "CONNECTION STRING"
MONGODB_DATABASE = "DATABASE NAME"
```

You should also add this env to your Vercel Enviroment variables.

### MongoDB Setup

For MongoDB, make sure you have the following collections:
-users
-boxes
-files
-ratelimits

The `ratelimits` collection should be a `Time Series` collection with the following configuration:

```yaml
timeField: expireAt
expireAfterSeconds: 5
```
