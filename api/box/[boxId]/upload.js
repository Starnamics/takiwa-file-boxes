import clientPromise from "../../../lib/mongodb";
import responses from "../../../lib/errors";
import { ObjectId } from "mongodb";
import isRequestRatelimitted from "../../../lib/ratelimiter";
import multiparty from "multiparty";
import fs from "fs";

export default async function handler(request, response) {
	if (await isRequestRatelimitted(request, 10)) {
		return response.status(429).json(responses.TOO_MANY_REQUESTS);
	}
	try {
		if (request.method !== "POST") {
			return response.status(405).json(responses.INVALID_METHOD);
		}

		let { boxId } = request.query;

		const form = new multiparty.Form();
		const data = await new Promise((resolve, reject) => {
			form.parse(request, function (err, fields, files) {
				if (err) reject({ err });
				resolve({ fields, files });
			});
		});

		let csvFile;

		if (data.files.boxFile) {
			if (
				data.files.boxFile[0].headers["content-type"] !== "text/csv" &&
				data.files.boxFile[0].headers["content-type"] !== "application/csv"
			) {
				return response.status(400).json(responses.INVALID_FILE_TYPE);
			}

			if (data.files.boxFile[0].size >= 2.5e6) {
				return response.status(413).json(responses.FILE_TOO_LARGE);
			}

			csvFile = fs.readFileSync(data.files.boxFile[0].path);
		}

		const client = await clientPromise;
		const db = client.db(process.env.MONGODB_DATABASE);
		const filesCollection = db.collection("files");
		const userCollection = db.collection("users");

		let user;
		let userId;

		if (request.cookies.AUTHORIZATION && request.cookies.USER_ID) {
			try {
				userId = new ObjectId(request.cookies.USER_ID);
				user = await userCollection.findOne({
					hashedPassword: request.cookies.AUTHORIZATION,
					_id: userId,
				});
				if (!user) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}
			} catch {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}
		}

		if (!user) {
			return response.status(401).json(responses.AUTHORIZATION_FAILED);
		}

		let result;
		try {
			const boxObjectId = new ObjectId(boxId);

			result = await filesCollection.findOne({ _id: boxObjectId });

			if (!result) {
				return response.status(404).json(responses.BOX_NOT_FOUND);
			}

			if (!user.isSuperUser && (!result.owner || !result.owner.id)) {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}

			if (
				user.isSuperUser !== true &&
				result.owner.id.toString() !== userId.toString()
			) {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}

			await filesCollection.updateOne(
				{
					_id: boxObjectId,
				},
				{ $set: { file: csvFile } }
			);
		} catch (e) {
			return response.status(404).json(responses.BOX_NOT_FOUND);
		}

		return response.status(200).json({
			status: 200,
			message: "File Uploaded",
			success: true,
			file: result.file,
		});
	} catch (e) {
		return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
	}
}
