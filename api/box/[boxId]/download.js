import clientPromise from "../../../lib/mongodb";
import responses from "../../../lib/errors";
import { ObjectId } from "mongodb";
import isRequestRatelimitted from "../../../lib/ratelimiter";

export default async function handler(request, response) {
	if (await isRequestRatelimitted(request, 5)) {
		return response.status(429).json(responses.TOO_MANY_REQUESTS);
	}
	try {
		if (request.method !== "GET") {
			return response.status(405).json(responses.INVALID_METHOD);
		}

		let { boxId } = request.query;

		const client = await clientPromise;
		const db = client.db(process.env.MONGODB_DATABASE);
		const filesCollection = db.collection("files");
		const boxesCollection = db.collection("boxes");
		const userCollection = db.collection("users");

		let user;
		let userId;

		if (request.cookies.AUTHORIZATION && request.cookies.USER_ID) {
			try {
				userId = new ObjectId(request.cookies.USER_ID);
				user = await userCollection.findOne({
					hashedPassword: request.cookies.AUTHORIZATION,
					_id: userId,
				});
				if (!user) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}
			} catch {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}
		}

		let result;
		try {
			const boxObjectId = new ObjectId(boxId);
			result = await filesCollection.findOne({
				_id: boxObjectId,
			});
		} catch {
			return response.status(404).json(responses.BOX_NOT_FOUND);
		}

		if (!result) {
			return response.status(404).json(responses.BOX_NOT_FOUND);
		}

		if (!result.file) {
			return response.status(404).json(responses.BOX_NOT_FOUND);
		}

		if (result?.visibility === 2 && result.owner && result.owner.id) {
			if (
				result.owner.id.toString() !== userId.toString() &&
				!result.sharedWith.find(
					(e) => e.toString() === userId.toString()
				) &&
				!user.isSuperUser
			) {
				return response.status(404).json(responses.BOX_NOT_FOUND);
			}
		}

		try {
			const boxObjectId = new ObjectId(boxId);
			boxesCollection.updateOne(
				{ _id: boxObjectId },
				{ $inc: { downloads: 1 } }
			);
		} catch {}

		response.setHeader("Content-Type", "text/csv");
		response.setHeader(
			"Content-Disposition",
			`attachment; filename=${result.name}.csv`
		);

		return response.status(200).send(result.file.toString());
	} catch (e) {
		return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
	}
}
