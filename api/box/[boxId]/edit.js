import clientPromise from "../../../lib/mongodb";
import responses from "../../../lib/errors";
import { ObjectId } from "mongodb";
import isRequestRatelimitted from "../../../lib/ratelimiter";

export default async function handler(request, response) {
	if (await isRequestRatelimitted(request, 10)) {
		return response.status(429).json(responses.TOO_MANY_REQUESTS);
	}
	try {
		if (request.method !== "POST") {
			return response.status(405).json(responses.INVALID_METHOD);
		}

		let body = JSON.parse(request.body);
		let boxName = body.boxName;
		let boxDescription = body.boxDescription;
		let boxVisibility = body.boxVisibility;

		boxName = boxName.split("\\").join("");
		boxName = boxName.split("<").join("");
		boxName = boxName.split(">").join("");
		boxName = boxName.split("/").join("");
		boxName = boxName.split("  ").join("");
		boxName = boxName.trim();
		boxName = boxName.slice(0, 32);

		boxDescription = boxDescription.split("\\").join("");
		boxDescription = boxDescription.split("<").join("");
		boxDescription = boxDescription.split(">").join("");
		boxDescription = boxDescription.split("  ").join("");
		boxDescription = boxDescription.trim();
		boxDescription = boxDescription.slice(0, 500);

		if (isNaN(boxVisibility) || boxVisibility < 1 || boxVisibility > 2) {
			boxVisibility = 2;
		}

		if (boxName.length < 4 || boxName.length > 32) {
			return response.status(400).json(responses.VALIDATION_FAILED);
		}

		if (boxDescription.length > 500) {
			return response.status(400).json(responses.VALIDATION_FAILED);
		}

		let { boxId } = request.query;

		const client = await clientPromise;
		const db = client.db(process.env.MONGODB_DATABASE);
		const userCollection = db.collection("users");
		const boxesCollection = db.collection("boxes");

		let user;
		let userId;

		if (request.cookies.AUTHORIZATION && request.cookies.USER_ID) {
			try {
				userId = new ObjectId(request.cookies.USER_ID);
				user = await userCollection.findOne({
					hashedPassword: request.cookies.AUTHORIZATION,
					_id: userId,
				});
				if (!user) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}
			} catch {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}
		}

		if (!user) {
			return response.status(401).json(responses.AUTHORIZATION_FAILED);
		}

		let result;
		try {
			const boxObjectId = new ObjectId(boxId);

			result = await boxesCollection.findOne({ _id: boxObjectId });

			if (!result) {
				return response.status(404).json(responses.BOX_NOT_FOUND);
			}

			if (!user.isSuperUser && (!result.owner || !result.owner.id)) {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}

			if (
				user.isSuperUser !== true &&
				result.owner.id.toString() !== userId.toString()
			) {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}

			await boxesCollection.updateOne(
				{
					_id: boxObjectId,
				},
				{
					$set: {
						name: boxName,
						description: boxDescription,
						visibility: boxVisibility,
					},
				}
			);
		} catch (e) {
			return response.status(404).json(responses.BOX_NOT_FOUND);
		}

		return response.status(200).json({
			status: 200,
			message: "Box Edited",
			success: true,
		});
	} catch (e) {
		return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
	}
}
