import clientPromise from "../lib/mongodb";
import multiparty from "multiparty";
import fs from "fs";
import responses from "../lib/errors";
import { ObjectId } from "mongodb";
import isRequestRatelimitted from "../lib/ratelimiter";

export default async function handler(request, response) {
	if (await isRequestRatelimitted(request, 1)) {
		return response.status(429).json(responses.TOO_MANY_REQUESTS);
	}
	try {
		if (request.method !== "POST") {
			return response.status(405).json(responses.INVALID_METHOD);
		}

		const form = new multiparty.Form();
		const data = await new Promise((resolve, reject) => {
			form.parse(request, function (err, fields, files) {
				if (err) reject({ err });
				resolve({ fields, files });
			});
		});

		let boxName = data.fields.boxName[0];
		let boxDescription = data.fields.boxDescription[0];
		let boxVisibility = data.fields.boxVisibility[0];

		if (!boxName) {
			return response.status(400).json(responses.VALIDATION_FAILED);
		}
		if (!boxDescription) {
			boxDescription = "";
		}
		if (!boxVisibility) {
			return response.status(400).json(responses.VALIDATION_FAILED);
		}

		boxName = boxName.toString();
		boxDescription = boxDescription.toString();

		boxVisibility = parseInt(boxVisibility);

		if (isNaN(boxVisibility) || boxVisibility <= 0 || boxVisibility > 2) {
			return response.status(400).json(responses.VALIDATION_FAILED);
		}

		boxName = boxName.split("\\").join("");
		boxName = boxName.split("<").join("");
		boxName = boxName.split(">").join("");
		boxName = boxName.split("/").join("");
		boxName = boxName.split("  ").join("");
		boxName = boxName.trim();
		boxName = boxName.slice(0, 32);

		boxDescription = boxDescription.split("\\").join("");
		boxDescription = boxDescription.split("<").join("");
		boxDescription = boxDescription.split(">").join("");
		boxDescription = boxDescription.split("  ").join("");
		boxDescription = boxDescription.trim();
		boxDescription = boxDescription.slice(0, 500);

		if (boxName.length < 4 || boxName.length > 32) {
			return response.status(400).json(responses.VALIDATION_FAILED);
		}

		if (boxDescription.length > 500) {
			return response.status(400).json(responses.VALIDATION_FAILED);
		}

		let csvFile;

		if (data.files.boxFile) {
			if (
				data.files.boxFile[0].headers["content-type"] !== "text/csv" &&
				data.files.boxFile[0].headers["content-type"] !== "application/csv"
			) {
				return response.status(400).json(responses.INVALID_FILE_TYPE);
			}

			if (data.files.boxFile[0].size >= 2.5e6) {
				return response.status(413).json(responses.FILE_TOO_LARGE);
			}

			csvFile = fs.readFileSync(data.files.boxFile[0].path);
		}

		const client = await clientPromise;
		const db = client.db(process.env.MONGODB_DATABASE);
		const boxesCollection = db.collection("boxes");
		const filesCollection = db.collection("files");
		const userCollection = db.collection("users");

		let user;
		let userId;

		let username;

		if (request.cookies.AUTHORIZATION && request.cookies.USER_ID) {
			try {
				userId = new ObjectId(request.cookies.USER_ID);
				user = await userCollection.findOne({
					hashedPassword: request.cookies.AUTHORIZATION,
					_id: userId,
				});

				username = user.username;

				if (!user) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}
			} catch {
				return response.status(401).json(responses.AUTHORIZATION_FAILED);
			}
		}

		function getOwnerDict() {
			if (user) {
				return {
					username: username,
					id: userId,
				};
			} else {
				return null;
			}
		}

		let fileResult;

		if (getOwnerDict()) {
			fileResult = await filesCollection.insertOne({
				file: csvFile,
				visibility: boxVisibility,
				owner: getOwnerDict(),
			});
		} else {
			fileResult = await filesCollection.insertOne({
				file: csvFile,
				visibility: 1,
				owner: null,
			});
		}

		if (!fileResult || fileResult.acknowledged !== true) {
			return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
		}

		if (!userId) {
			boxVisibility = 1;
		}

		const result = await boxesCollection.insertOne({
			_id: new ObjectId(fileResult.insertedId),
			name: boxName,
			visibility: boxVisibility,
			owner: getOwnerDict(),
			description: boxDescription,
			sharedWith: [],
			creationDate: Date.now(),
			views: 0,
			downloads: 0,
		});

		if (!result || result.acknowledged !== true) {
			return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
		}

		if (user) {
			try {
				await userCollection.updateOne(
					{ hashedPassword: request.cookies.AUTHORIZATION, _id: userId },
					{
						$push: { boxes: result.insertedId },
					}
				);
			} catch {}
		}

		return response.status(200).json({
			status: 200,
			message: "Box Created",
			success: true,
			boxId: result.insertedId,
		});
	} catch {
		return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
	}
}
