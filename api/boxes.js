import clientPromise from "../lib/mongodb";
import responses from "../lib/errors";
import isRequestRatelimitted from "../lib/ratelimiter";
import { ObjectId } from "mongodb";

export default async function handler(request, response) {
	if (await isRequestRatelimitted(request, 10)) {
		return response.status(429).json(responses.TOO_MANY_REQUESTS);
	}
	try {
		if (request.method !== "GET") {
			return response.status(405).json(responses.INVALID_METHOD);
		}

		const client = await clientPromise;
		const db = client.db(process.env.MONGODB_DATABASE);
		const boxesCollection = db.collection("boxes");
		const userCollection = db.collection("users");

		let user;
		let userId;

		async function authenticate() {
			if (request.cookies.AUTHORIZATION && request.cookies.USER_ID) {
				try {
					userId = new ObjectId(request.cookies.USER_ID);
					user = await userCollection.findOne({
						hashedPassword: request.cookies.AUTHORIZATION,
						_id: userId,
					});
					if (!user) {
						return false;
					}
				} catch {
					return false;
				}

				return true;
			}

			if (!user || !userId) {
				return false;
			}
		}

		async function getResult() {
			let result;
			if (request?.query?.type === "private") {
				const auth = await authenticate();
				if (auth !== true) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}

				if (!user || !userId) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}

				try {
					result = await boxesCollection
						.find({
							$or: [
								{
									"owner.id": userId,
								},
								{
									sharedWith: { $all: [userId] },
								},
							],
						})
						.toArray();
				} catch (e) {
					return response.status(404).json(responses.BOX_NOT_FOUND);
				}
			} else if (request?.query?.type === "public") {
				try {
					result = await boxesCollection
						.find({
							visibility: 1,
						})
						.toArray();
				} catch (e) {
					return response.status(404).json(responses.BOX_NOT_FOUND);
				}
			} else if (request?.query?.type === "all") {
				const auth = await authenticate();

				if (auth !== true) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}

				if (!user || user.isSuperUser !== true) {
					return response.status(401).json(responses.AUTHORIZATION_FAILED);
				}

				try {
					result = await boxesCollection.find().toArray();
				} catch (e) {
					return response.status(404).json(responses.BOX_NOT_FOUND);
				}
			}

			return result;
		}

		return response.status(200).json({
			status: 200,
			message: "Boxes Fetched",
			success: true,
			boxes: await getResult(),
		});
	} catch (e) {
		return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
	}
}
