import clientPromise from "../lib/mongodb";
import responses from "../lib/errors";
import crypto from "bcrypt";
import isRequestRatelimitted from "../lib/ratelimiter";
import cookie from "cookie";

export default async function handler(request, response) {
	if (await isRequestRatelimitted(request, 1)) {
		return response.status(429).json(responses.TOO_MANY_REQUESTS);
	}
	try {
		if (request.method !== "POST") {
			return response.status(405).json(responses.INVALID_METHOD);
		}

		const client = await clientPromise;
		const db = client.db(process.env.MONGODB_DATABASE);
		const userCollection = db.collection("users");

		let body = JSON.parse(request.body);

		let username = body.username;
		let password = body.password;

		username = username.toString();
		password = password.toString();

		username = username.replace(/[\W_]+/g, " ");
		username = username.trim();

		if (!username || !password) {
			return response.status(400).json(responses.INVALID_REGISTRATION_INPUT);
		}

		if (username.length < 4 || username.length > 30) {
			return response.status(400).json(responses.INVALID_REGISTRATION_INPUT);
		}

		if (password.length < 8 || password.length > 2048) {
			return response.status(400).json(responses.INVALID_REGISTRATION_INPUT);
		}

		const userResult = await userCollection.findOne({
			username: username,
		});

		if (!userResult || !userResult?.hashedPassword || !userResult?.username) {
			return response.status(404).json(responses.USER_NOT_FOUND);
		}

		let isPasswordValid = await crypto.compare(
			password,
			userResult?.hashedPassword
		);

		if (!isPasswordValid) {
			return response.status(403).json(responses.INCORRECT_PASSWORD);
		}

		const userId = userResult._id;

		let userIdCookie = cookie.serialize("USER_ID", userId, {
			path: "/",
			maxAge: 60 * 60 * 24 * 7,
		});

		let authorizationCookie = cookie.serialize(
			"AUTHORIZATION",
			userResult.hashedPassword,
			{
				path: "/",
				maxAge: 60 * 60 * 24 * 7,
			}
		);

		let cookieArray = [];

		cookieArray.push(userIdCookie, authorizationCookie);

		if (userResult?.isSuperUser === true) {
			let superUserCookie = cookie.serialize("SUPER_USER", true, {
				path: "/",
				maxAge: 60 * 60 * 24 * 7,
			});
			cookieArray.push(superUserCookie);
		}

		response.setHeader("Set-Cookie", cookieArray);

		return response.status(200).json({
			status: 200,
			message: "User Logged In",
			success: true,
		});
	} catch (e) {
		return response.status(500).json(responses.INTERNAL_SERVER_ERROR);
	}
}
