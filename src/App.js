import Navbar from "./components/navbar";
import BoxGrid from "./components/boxGrid";

export default function Home({ isConnected }) {
	return (
		<div>
			<Navbar active="public"></Navbar>
			<BoxGrid type="public"></BoxGrid>
		</div>
	);
}
