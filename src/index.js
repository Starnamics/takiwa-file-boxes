import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "./App";
import Box from "./routes/box";
import MyBoxes from "./routes/my";
import AllBoxes from "./routes/all";
import "semantic-ui-css/semantic.css";

// import ROUTE from "./routes/ROUTE"

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	<BrowserRouter>
		<Routes>
			<Route path="/" element={<App />} />
			<Route path="/private" element={<MyBoxes />} />
			<Route path="/all" element={<AllBoxes />} />
			<Route path=":boxId" element={<Box />} />
		</Routes>
	</BrowserRouter>
);
