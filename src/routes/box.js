import Navbar from "../components/navbar";
import BoxLoader from "../components/boxLoader";
import { useParams } from "react-router-dom";
import useFetch from "react-fetch-hook";
import { Dimmer, Loader } from "semantic-ui-react";

export default function Box() {
	let { boxId } = useParams();

	const { isLoading, data } = useFetch(`api/box/${boxId}`);

	return (
		<div>
			<Navbar></Navbar>
			{isLoading ? (
				<Dimmer active>
					<Loader indeterminate>Fetching Box</Loader>
				</Dimmer>
			) : (
				<BoxLoader data={data}></BoxLoader>
			)}
		</div>
	);
}
