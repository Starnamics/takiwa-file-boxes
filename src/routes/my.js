import Navbar from "../components/navbar";
import BoxGrid from "../components/boxGrid";

export default function MyBoxes({ isConnected }) {
	return (
		<div>
			<Navbar active="private"></Navbar>
			<BoxGrid type="private"></BoxGrid>
		</div>
	);
}
