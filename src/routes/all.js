import Navbar from "../components/navbar";
import BoxGrid from "../components/boxGrid";

export default function AllBoxes({ isConnected }) {
	return (
		<div>
			<Navbar active="all"></Navbar>
			<BoxGrid type="all"></BoxGrid>
		</div>
	);
}
