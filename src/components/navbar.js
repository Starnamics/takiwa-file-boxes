import { Menu, Segment } from "semantic-ui-react";
import { useState } from "react";
import CreateModal from "./createModal";
import RegisterModal from "./registerModal";
import LoginModal from "./loginModal";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";

export default function Navbar(props) {
	const [IsCreateModalOpen, SetCreateModalState] = useState(false);
	const [IsRegisterModalOpen, SetRegisterModalState] = useState(false);
	const [IsLoginModalOpen, SetLoginModalState] = useState(false);

	const navigate = useNavigate();

	function IsActive(key) {
		return props.active === key;
	}

	function IsLoggedIn() {
		let isOwner = false;
		if (Cookies.get("USER_ID") && Cookies.get("AUTHORIZATION")) {
			isOwner = true;
		}
		return isOwner;
	}

	function IsSuperUser() {
		let isOwner = false;
		if (Cookies.get("SUPER_USER") === "true") {
			isOwner = true;
		}
		return isOwner;
	}

	return (
		<>
			<Segment
				inverted
				style={{
					borderRadius: 0,
					boxShadow: `0px 2px 20px rgba(22,22,22,0.7)`,
				}}>
				<Menu inverted secondary>
					<Menu.Item
						active={IsActive("public")}
						onClick={() => navigate("/")}>
						Public Boxes
					</Menu.Item>
					{IsLoggedIn() ? (
						<Menu.Item
							active={IsActive("private")}
							onClick={() => navigate("/private")}>
							My Boxes
						</Menu.Item>
					) : (
						<></>
					)}
					{IsSuperUser() && IsLoggedIn() ? (
						<Menu.Item
							active={IsActive("all")}
							onClick={() => navigate("/all")}>
							All Boxes
						</Menu.Item>
					) : (
						<></>
					)}
					<Menu.Menu position="right">
						<Menu.Item
							color="blue"
							active
							onClick={() => SetCreateModalState(true)}>
							Create
						</Menu.Item>
						{Cookies.get("AUTHORIZATION") ? (
							<Menu.Item
								onClick={() => {
									Cookies.set("USER_ID", null, {
										maxAge: 0,
										expires: 0,
									});
									Cookies.set("AUTHORIZATION", null, {
										maxAge: 0,
										expires: 0,
									});
									Cookies.set("SUPER_USER", null, {
										maxAge: 0,
										expires: 0,
									});
									window.location.reload();
								}}>
								Logout
							</Menu.Item>
						) : (
							<>
								<Menu.Item onClick={() => SetRegisterModalState(true)}>
									Register
								</Menu.Item>
								<Menu.Item onClick={() => SetLoginModalState(true)}>
									Login
								</Menu.Item>
							</>
						)}
					</Menu.Menu>
				</Menu>
			</Segment>
			<CreateModal
				IsOpen={IsCreateModalOpen}
				SetOpen={SetCreateModalState}
			/>
			<RegisterModal
				IsOpen={IsRegisterModalOpen}
				SetOpen={SetRegisterModalState}
			/>
			<LoginModal IsOpen={IsLoginModalOpen} SetOpen={SetLoginModalState} />
		</>
	);
}
