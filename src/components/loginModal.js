import { Modal, Button, Input, Message } from "semantic-ui-react";
import { useState } from "react";

export default function LoginModal(props) {
	const IsRegisterModalOpen = props.IsOpen;
	const SetRegisterModalState = props.SetOpen;

	const [IsPasswordErrorVisible, SetPasswordErrorVisible] = useState(false);
	const [IsUsernameErrorVisible, SetUsernameErrorVisible] = useState(false);
	const [IsRegisteringAccount, SetRegisteringAccount] = useState(false);
	const [IsRegistrationDisabled, SetRegistrationDisabled] = useState(true);
	const [IsRegistrationSuccess, SetRegistrationSuccess] = useState(false);
	const [IsResponseErrorVisible, SetResponseErrorVisible] = useState(false);
	const [ResponseErrorText, SetResponseErrorText] = useState(false);

	const [UsernameValue, SetUsernameValue] = useState("");
	const [PasswordValue, SetPasswordValue] = useState("");
	function AreFieldsValid(values) {
		let isValid = true;

		SetRegistrationDisabled(false);

		if (!IsPasswordErrorVisible && !IsUsernameErrorVisible) {
			isValid = false;
		}

		if (values.username.length < 4 || values.username.length > 30) {
			isValid = false;
			SetRegistrationDisabled(true);
		}

		if (values.password.length < 8 || values.password.length > 2048) {
			isValid = false;
			SetRegistrationDisabled(true);
		}

		if (isValid) {
			SetRegistrationDisabled(false);
		}

		return isValid;
	}

	function ValidateUsername(value) {
		if (!value) {
			value = "";
		}
		value = value.slice(0, 30);
		value = value.replace(/[^a-z0-9_-]$/gim, "");
		value = value.trim();
		if (value.length < 4 || value.length > 30) {
			SetUsernameErrorVisible(true);
		} else {
			SetUsernameErrorVisible(false);
		}

		SetUsernameValue(value);

		AreFieldsValid({
			username: value,
			password: PasswordValue,
		});
	}

	function ValidatePassword(value) {
		if (!value) {
			value = "";
		}
		value = value.slice(0, 2048);
		if (value.length < 8 || value.length > 2048) {
			SetPasswordErrorVisible(true);
		} else {
			SetPasswordErrorVisible(false);
		}

		SetPasswordValue(value);

		AreFieldsValid({
			username: UsernameValue,
			password: value,
		});
	}

	async function RegisterAccount() {
		SetRegistrationSuccess(false);
		SetResponseErrorVisible(false);
		AreFieldsValid({
			username: UsernameValue,
			password: PasswordValue,
		});

		if (IsRegistrationDisabled) {
			return;
		}

		SetRegisteringAccount(true);
		SetRegistrationDisabled(true);

		const response = await fetch("api/login", {
			body: JSON.stringify({
				username: UsernameValue,
				password: PasswordValue,
			}),
			method: "POST",
		});

		const body = await response.json();
		SetRegisteringAccount(false);

		if (!body) {
			SetResponseErrorVisible(true);
			SetResponseErrorText("An unknown error occured");
			SetRegistrationDisabled(false);
			return;
		} else if (!body.success && body.errors && body.errors[0]) {
			SetResponseErrorVisible(true);
			SetResponseErrorText(body.errors[0].user_facing_message);
			SetRegistrationDisabled(false);
			return;
		}

		if (body.success) {
			SetRegistrationSuccess(true);
			window.location.reload();
		}
	}

	return (
		<Modal size={"mini"} open={IsRegisterModalOpen}>
			<Modal.Header>Login</Modal.Header>
			<Modal.Content>
				{IsRegistrationSuccess ? (
					<Message positive>
						<Message.Header>Logged In!</Message.Header>
						<p>Redirecting you, please wait</p>
					</Message>
				) : (
					<></>
				)}

				{IsUsernameErrorVisible ? (
					<Message negative>
						<Message.Header>Invalid Username</Message.Header>
						<p>Your username must be at least 3 characters long</p>
					</Message>
				) : (
					<></>
				)}

				{IsResponseErrorVisible ? (
					<Message negative>
						<Message.Header>Failed to login</Message.Header>
						<p>{ResponseErrorText}</p>
					</Message>
				) : (
					<></>
				)}

				{IsPasswordErrorVisible ? (
					<Message negative>
						<Message.Header>Invalid Password</Message.Header>
						<p>Your password must be at least 8 characters long</p>
					</Message>
				) : (
					<></>
				)}

				<Input
					placeholder="Username"
					fluid={true}
					style={{ marginBottom: "1rem" }}
					disabled={IsRegisteringAccount}
					error={IsUsernameErrorVisible}
					onChange={(e, value) => {
						ValidateUsername(value.value);
					}}
					value={UsernameValue}
				/>
				<Input
					placeholder="Password"
					type="password"
					fluid={true}
					disabled={IsRegisteringAccount}
					style={{ marginBottom: "1rem" }}
					error={IsPasswordErrorVisible}
					onChange={(e, value) => {
						ValidatePassword(value.value);
					}}
					value={PasswordValue}
				/>
			</Modal.Content>
			<Modal.Actions>
				<Button
					secondary
					disabled={IsRegisteringAccount}
					onClick={() => {
						SetPasswordErrorVisible(false);
						SetUsernameErrorVisible(false);
						SetPasswordValue("");
						SetUsernameValue("");
						SetRegistrationDisabled(true);
						SetRegisterModalState(false);
					}}>
					Cancel
				</Button>
				<Button
					primary
					disabled={IsRegisteringAccount || IsRegistrationDisabled}
					loading={IsRegisteringAccount}
					onClick={() => RegisterAccount()}>
					Login
				</Button>
			</Modal.Actions>
		</Modal>
	);
}
