import {
	Modal,
	Button,
	Input,
	Message,
	TextArea,
	Form,
	Dropdown,
} from "semantic-ui-react";
import { useState } from "react";

const boxTypeOptions = [
	{ key: 1, text: "Public", value: 1 },
	{ key: 2, text: "Private", value: 2 },
];

export default function EditModal(props) {
	const IsCreateModalOpen = props.IsOpen;
	const SetCreateModalState = props.SetOpen;
	const [SelectedBoxType, SetBoxType] = useState(1);
	const [InputtedBoxName, SetBoxName] = useState("");
	const [InputtedBoxDescription, SetBoxDescription] = useState("");
	const [IsBoxNameValidating, SetBoxNameValidating] = useState(false);
	const [IsBoxNameErrorVisible, SetBoxNameErrorVisible] = useState(false);
	const [BoxNameValidationSuccess, SetBoxNameValidationSuccess] =
		useState(true);
	const [IsCreatingBox, SetCreatingBox] = useState(false);
	const [ResponseErrorMessage, SetResponseErrorMessage] =
		useState("Unknown Error");
	const [ResponseErrorVisible, SetResponseErrorVisible] = useState(false);
	const [SuccessMessageVisible, SetSuccessMessageVisible] = useState(false);

	function ValidateBoxName(value) {
		if (value.length < 4 || value.length > 32) {
			SetBoxNameValidationSuccess(false);
			SetBoxNameErrorVisible(true);
		} else {
			SetBoxNameValidationSuccess(true);
			SetBoxNameErrorVisible(false);
		}
		let updatedValue = value;
		updatedValue = updatedValue.replaceAll("\\", "");
		updatedValue = updatedValue.replaceAll("<", "");
		updatedValue = updatedValue.replaceAll(">", "");
		updatedValue = updatedValue.replaceAll("/", "");
		updatedValue = updatedValue.replaceAll("  ", " ");
		updatedValue = updatedValue.trimStart();
		updatedValue = updatedValue.slice(0, 32);
		SetBoxName(updatedValue);
		return updatedValue;
	}

	function ValidateBoxDescription(value) {
		let updatedValue = value;
		updatedValue = updatedValue.replaceAll("\\", "");
		updatedValue = updatedValue.replaceAll("<", "");
		updatedValue = updatedValue.replaceAll(">", "");
		updatedValue = updatedValue.replaceAll("  ", " ");
		updatedValue = updatedValue.trimStart();
		updatedValue = updatedValue.slice(0, 500);
		SetBoxDescription(updatedValue);
		return updatedValue;
	}

	function HandleCreateModalClose() {
		SetBoxType(1);
		SetBoxName("");
		SetBoxNameValidating(false);
		SetBoxNameValidationSuccess(true);
		SetCreateModalState(false);
	}

	async function HandleCreateBoxSubmit() {
		if (!InputtedBoxName) {
			SetBoxNameErrorVisible(true);
			return;
		} else {
			SetBoxNameErrorVisible(false);
		}

		ValidateBoxName(InputtedBoxName);
		ValidateBoxDescription(InputtedBoxDescription);

		if (InputtedBoxName.length <= 3) {
			SetBoxNameErrorVisible(true);
			return;
		} else {
			SetBoxNameErrorVisible(false);
		}

		SetCreatingBox(true);

		const request = await fetch(`/api/box/${props?.data?.box?._id}/edit`, {
			method: "POST",
			body: JSON.stringify({
				boxName: InputtedBoxName,
				boxDescription: InputtedBoxDescription,
				boxVisibility: SelectedBoxType,
			}),
		});
		const body = await request.json();

		if (!request || request?.status !== 200 || !body?.success) {
			SetResponseErrorVisible(true);
			SetResponseErrorMessage(
				body?.errors[0]?.user_facing_message || "Unknown Error"
			);
			SetCreatingBox(false);
			return;
		} else {
			SetResponseErrorVisible(false);
			SetSuccessMessageVisible(true);
		}

		SetCreatingBox(false);
		window.location.reload();
	}

	return (
		<Modal size={"mini"} open={IsCreateModalOpen}>
			<Modal.Header>Edit Box</Modal.Header>
			<Modal.Content>
				{SuccessMessageVisible ? (
					<Message positive>
						<Message.Header>Box Edited!</Message.Header>
						<p>Reloading page, please wait</p>
					</Message>
				) : (
					<></>
				)}
				{IsBoxNameErrorVisible ? (
					<Message negative>
						<Message.Header>Invalid Box Name</Message.Header>
						<p>Your box name must be between 4 and 32 characters</p>
					</Message>
				) : (
					<></>
				)}
				{ResponseErrorVisible ? (
					<Message negative>
						<Message.Header>Failed to create box</Message.Header>
						<p>{ResponseErrorMessage}</p>
					</Message>
				) : (
					<></>
				)}
				<Input
					placeholder="Box Name"
					fluid
					style={{ marginBottom: "1rem" }}
					error={!BoxNameValidationSuccess || IsBoxNameErrorVisible}
					loading={IsBoxNameValidating}
					onChange={(e, value) => {
						ValidateBoxName(value.value);
					}}
					value={InputtedBoxName}
				/>
				<Form>
					<TextArea
						placeholder="Box Description"
						fluid
						onChange={(e, value) => {
							ValidateBoxDescription(value.value);
						}}
						value={InputtedBoxDescription}
						style={{
							minHeight: 100,
							marginBottom: "1rem",
							maxHeight: 300,
						}}
					/>
				</Form>
				<Dropdown
					onChange={(e, value) => SetBoxType(value.value)}
					options={boxTypeOptions}
					placeholder="Box Visibility"
					selection
					fluid
					value={SelectedBoxType}
				/>
			</Modal.Content>
			<Modal.Actions>
				<Button
					secondary
					disabled={IsCreatingBox}
					onClick={() => {
						HandleCreateModalClose();
					}}>
					Cancel
				</Button>
				<Button
					primary
					disabled={IsCreatingBox}
					loading={IsCreatingBox}
					onClick={() => {
						HandleCreateBoxSubmit();
					}}>
					Edit
				</Button>
			</Modal.Actions>
		</Modal>
	);
}
