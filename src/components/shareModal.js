import { Modal, Button, Input, Message } from "semantic-ui-react";
import { useState } from "react";

export default function ShareModal(props) {
	const IsCreateModalOpen = props.IsOpen;
	const SetCreateModalState = props.SetOpen;
	const [InputtedUsername, SetInputtedUsername] = useState("");
	const [IsUsernameValidating, SetUsernameValidating] = useState(false);
	const [IsUsernameErrorVisible, SetUsernameErrorVisible] = useState(false);
	const [UsernameValidationSuccess, SetUsernameValidationSuccess] =
		useState(true);
	const [IsCreatingBox, SetCreatingBox] = useState(false);
	const [ResponseErrorMessage, SetResponseErrorMessage] =
		useState("Unknown Error");
	const [ResponseErrorVisible, SetResponseErrorVisible] = useState(false);
	const [SuccessMessageVisible, SetSuccessMessageVisible] = useState(false);

	function HandleCreateModalClose() {
		SetInputtedUsername("");
		SetUsernameErrorVisible(false);
		SetUsernameValidating(false);
		SetUsernameValidationSuccess(true);
		SetCreateModalState(false);
	}

	function ValidateUsername(value) {
		if (!value) {
			value = "";
		}
		value = value.slice(0, 30);
		value = value.replace(/[^a-z0-9_-]$/gim, "");
		value = value.trim();
		if (value.length < 4 || value.length > 30) {
			SetUsernameErrorVisible(true);
		} else {
			SetUsernameErrorVisible(false);
		}

		SetInputtedUsername(value);
	}

	async function HandleCreateBoxSubmit() {
		if (!InputtedUsername) {
			SetUsernameErrorVisible(true);
			return;
		} else {
			SetUsernameErrorVisible(false);
		}
		ValidateUsername(InputtedUsername);

		SetCreatingBox(true);

		const request = await fetch(`/api/box/${props?.data?.box?._id}/share`, {
			method: "POST",
			body: JSON.stringify({
				username: InputtedUsername,
			}),
		});
		const body = await request.json();

		if (!request || request?.status !== 200 || !body?.success) {
			SetResponseErrorVisible(true);
			SetResponseErrorMessage(
				body?.errors[0]?.user_facing_message || "Unknown Error"
			);
			SetCreatingBox(false);
			return;
		} else {
			SetResponseErrorVisible(false);
			SetSuccessMessageVisible(true);
		}

		SetCreatingBox(false);
	}

	return (
		<Modal size={"mini"} open={IsCreateModalOpen}>
			<Modal.Header>Share Box</Modal.Header>
			<Modal.Content>
				{SuccessMessageVisible ? (
					<Message positive>
						<Message.Header>Box Shared!</Message.Header>
						<p>You can share the link to this box with the user now</p>
					</Message>
				) : (
					<></>
				)}
				{IsUsernameErrorVisible ? (
					<Message negative>
						<Message.Header>Invalid Username</Message.Header>
						<p>Usernames must be at between 3 and 30 characters long</p>
					</Message>
				) : (
					<></>
				)}
				{ResponseErrorVisible ? (
					<Message negative>
						<Message.Header>Failed to create box</Message.Header>
						<p>{ResponseErrorMessage}</p>
					</Message>
				) : (
					<></>
				)}
				<Input
					placeholder="Username"
					fluid
					error={!UsernameValidationSuccess || IsUsernameErrorVisible}
					loading={IsUsernameValidating}
					onChange={(e, value) => {
						ValidateUsername(value.value);
					}}
					value={InputtedUsername}
				/>
			</Modal.Content>
			<Modal.Actions>
				<Button
					secondary
					disabled={IsCreatingBox}
					onClick={() => {
						HandleCreateModalClose();
					}}>
					Cancel
				</Button>
				<Button
					primary
					disabled={IsCreatingBox}
					loading={IsCreatingBox}
					onClick={() => {
						HandleCreateBoxSubmit();
					}}>
					Share
				</Button>
			</Modal.Actions>
		</Modal>
	);
}
