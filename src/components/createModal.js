import {
	Modal,
	Button,
	Input,
	Dropdown,
	Message,
	TextArea,
	Form,
} from "semantic-ui-react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

const boxTypeOptions = [
	{ key: 1, text: "Public", value: 1 },
	{ key: 2, text: "Private", value: 2 },
];

export default function CreateModal(props) {
	const IsCreateModalOpen = props.IsOpen;
	const SetCreateModalState = props.SetOpen;
	const [SelectedBoxType, SetBoxType] = useState(1);
	const [InputtedBoxName, SetBoxName] = useState("");
	const [InputtedBoxDescription, SetBoxDescription] = useState("");
	const [InputtedBoxFile, SetBoxFile] = useState();
	const [IsBoxNameValidating, SetBoxNameValidating] = useState(false);
	const [IsFileSizeErrorVisible, SetFileSizeErrorVisible] = useState(false);
	const [IsFileTypeErrorVisible, SetFileTypeErrorVisible] = useState(false);
	const [IsBoxNameErrorVisible, SetBoxNameErrorVisible] = useState(false);
	const [BoxNameValidationSuccess, SetBoxNameValidationSuccess] =
		useState(true);
	const [IsCreatingBox, SetCreatingBox] = useState(false);
	const [IsBoxCreationDisabled, SetBoxCreationDisabled] = useState(true);
	const [ResponseErrorMessage, SetResponseErrorMessage] =
		useState("Unknown Error");
	const [ResponseErrorVisible, SetResponseErrorVisible] = useState(false);
	const [SuccessMessageVisible, SetSuccessMessageVisible] = useState(false);
	const [IsDropdownDisabled, SetDropdownDisabled] = useState(true);

	let navigate = useNavigate();

	function ValidateBoxName(value) {
		if (value.length < 4 || value.length > 32) {
			SetBoxNameValidationSuccess(false);
			SetBoxNameErrorVisible(true);
			SetBoxCreationDisabled(true);
		} else {
			SetBoxNameValidationSuccess(true);
			SetBoxNameErrorVisible(false);
			SetBoxCreationDisabled(false);
		}
		let updatedValue = value;
		updatedValue = updatedValue.replaceAll("\\", "");
		updatedValue = updatedValue.replaceAll("<", "");
		updatedValue = updatedValue.replaceAll(">", "");
		updatedValue = updatedValue.replaceAll("/", "");
		updatedValue = updatedValue.replaceAll("  ", " ");
		updatedValue = updatedValue.trimStart();
		updatedValue = updatedValue.slice(0, 32);
		SetBoxName(updatedValue);
		return updatedValue;
	}

	function ValidateBoxDescription(value) {
		let updatedValue = value;
		updatedValue = updatedValue.replaceAll("\\", "");
		updatedValue = updatedValue.replaceAll("<", "");
		updatedValue = updatedValue.replaceAll(">", "");
		updatedValue = updatedValue.replaceAll("  ", " ");
		updatedValue = updatedValue.trimStart();
		updatedValue = updatedValue.slice(0, 500);
		SetBoxDescription(updatedValue);
		return updatedValue;
	}

	function ValidateCSVFile(value) {
		if (value) {
			if (value.size >= 2.5e6) {
				SetFileSizeErrorVisible(true);
				SetBoxCreationDisabled(true);
				return false;
			}
			if (value.type !== "text/csv" && value.type !== "application/csv") {
				SetFileTypeErrorVisible(true);
				SetBoxCreationDisabled(true);
				return false;
			}
		}
		SetBoxCreationDisabled(false);
		SetFileSizeErrorVisible(false);
		SetBoxFile(value);
	}

	function HandleCreateModalClose() {
		SetBoxType(1);
		SetBoxName("");
		SetBoxFile();
		SetBoxNameValidating(false);
		SetBoxNameValidationSuccess(true);
		SetCreateModalState(false);
	}

	async function HandleCreateBoxSubmit() {
		if (!InputtedBoxName || !SelectedBoxType) {
			SetBoxNameErrorVisible(true);
			return;
		} else {
			SetBoxNameErrorVisible(false);
		}

		if (typeof SelectedBoxType !== "number") {
			return;
		}

		ValidateBoxName(InputtedBoxName);
		ValidateCSVFile(InputtedBoxFile);
		ValidateBoxDescription(InputtedBoxDescription);

		if (InputtedBoxName.length <= 3) {
			SetBoxNameErrorVisible(true);
			return;
		} else {
			SetBoxNameErrorVisible(false);
		}

		if (InputtedBoxFile && InputtedBoxFile.size >= 2.5e6) {
			SetFileSizeErrorVisible(true);
			return false;
		} else {
			SetFileSizeErrorVisible(false);
		}

		SetCreatingBox(true);

		const requestBody = new FormData();
		requestBody.append("boxName", InputtedBoxName);
		requestBody.append("boxVisibility", SelectedBoxType);
		requestBody.append("boxDescription", InputtedBoxDescription);
		if (InputtedBoxFile) {
			requestBody.append("boxFile", InputtedBoxFile);
		}

		const request = await fetch("/api/upload", {
			method: "POST",
			body: requestBody,
		});
		const body = await request.json();

		if (!request || request?.status !== 200 || !body?.success) {
			SetResponseErrorVisible(true);
			SetResponseErrorMessage(
				body?.errors[0]?.user_facing_message || "Unknown Error"
			);
			SetCreatingBox(false);
			SetBoxCreationDisabled(false);
			return;
		} else {
			SetResponseErrorVisible(false);
			SetSuccessMessageVisible(true);
		}

		SetCreatingBox(false);
		SetBoxCreationDisabled(true);
		navigate(`/${body.boxId}`, { replace: true });
		SetCreateModalState(false);
		SetSuccessMessageVisible(false);
	}

	useEffect(() => {
		if (Cookies.get("AUTHORIZATION")) {
			SetDropdownDisabled(false);
			SetBoxType(2);
		}
	}, []);

	return (
		<Modal size={"mini"} open={IsCreateModalOpen}>
			<Modal.Header>Create Box</Modal.Header>
			<Modal.Content>
				{SuccessMessageVisible ? (
					<Message positive>
						<Message.Header>Box Created!</Message.Header>
						<p>Redirecting you now, please wait</p>
					</Message>
				) : (
					<></>
				)}
				{IsBoxNameErrorVisible ? (
					<Message negative>
						<Message.Header>Invalid Box Name</Message.Header>
						<p>Your box name must be between 4 and 32 characters</p>
					</Message>
				) : (
					<></>
				)}
				{IsFileTypeErrorVisible ? (
					<Message negative>
						<Message.Header>Unallowed File Type</Message.Header>
						<p>You can only upload CSV files</p>
					</Message>
				) : (
					<></>
				)}
				{IsFileSizeErrorVisible ? (
					<Message negative>
						<Message.Header>Maximum File Size Exceeded</Message.Header>
						<p>The maximum allowed file size is 2MB</p>
					</Message>
				) : (
					<></>
				)}
				{ResponseErrorVisible ? (
					<Message negative>
						<Message.Header>Failed to create box</Message.Header>
						<p>{ResponseErrorMessage}</p>
					</Message>
				) : (
					<></>
				)}
				<Input
					placeholder="Box Name"
					fluid
					style={{ marginBottom: "1rem" }}
					error={!BoxNameValidationSuccess || IsBoxNameErrorVisible}
					loading={IsBoxNameValidating}
					onChange={(e, value) => {
						ValidateBoxName(value.value);
					}}
					value={InputtedBoxName}
				/>
				<Form>
					<TextArea
						placeholder="Box Description"
						fluid
						onChange={(e, value) => {
							ValidateBoxDescription(value.value);
						}}
						value={InputtedBoxDescription}
						style={{
							minHeight: 100,
							marginBottom: "1rem",
							maxHeight: 300,
						}}
					/>
				</Form>
				<Dropdown
					onChange={(e, value) => SetBoxType(value.value)}
					options={boxTypeOptions}
					placeholder="Box Visibility"
					selection
					fluid
					value={SelectedBoxType}
					disabled={IsDropdownDisabled}
				/>
				<Input
					style={{ marginTop: "1rem" }}
					type="file"
					fluid
					error={IsFileSizeErrorVisible}
					accept=".csv, text/csv, application/csv"
					onChange={(e, value) => {
						ValidateCSVFile(e.target.files[0]);
					}}></Input>
			</Modal.Content>
			<Modal.Actions>
				<Button
					secondary
					disabled={IsCreatingBox}
					onClick={() => {
						HandleCreateModalClose();
					}}>
					Cancel
				</Button>
				<Button
					primary
					disabled={IsCreatingBox || IsBoxCreationDisabled}
					loading={IsCreatingBox}
					onClick={() => {
						HandleCreateBoxSubmit();
					}}>
					Create
				</Button>
			</Modal.Actions>
		</Modal>
	);
}
