import {
	Header,
	Segment,
	Button,
	Icon,
	Table,
	Container,
	Pagination,
	Input,
} from "semantic-ui-react";
import { Buffer } from "buffer";
import { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import csvParse from "papaparse";
import Cookies from "js-cookie";

function saveFile(url, name) {
	return new Promise(function (resolve, reject) {
		let xhr = new XMLHttpRequest();
		xhr.responseType = "blob";
		xhr.onload = function () {
			resolve(xhr);
		};
		xhr.onerror = reject;
		xhr.open("GET", url);
		xhr.send();
	}).then(function (xhr) {
		let filename = name;
		let a = document.createElement("a");
		a.href = window.URL.createObjectURL(xhr.response);
		a.download = filename;
		a.style.display = "none";
		document.body.appendChild(a);
		a.click();
		a.remove();
		return xhr;
	});
}

export default function BoxFile(props) {
	const [IsFileDownloading, SetIsFileDownloading] = useState(false);
	const [ParsedChunks, SetParsedChunks] = useState();
	const [CurrentChunk, SetCurrentChunk] = useState();
	const [CurrentChunkId, SetCurrentChunkId] = useState(0);
	const [RawCSVData, SetRawCSVData] = useState();
	const [ChunkSize, SetChunkSize] = useState(50);
	const [IsFileUploading, SetIsFileUploading] = useState(false);
	const [IsFileDeleting, SetIsFileDeleting] = useState(false);

	const navigate = useNavigate();
	const fileUploadRef = useRef(null);

	function ChunkParsedData(size) {
		let chunkSize = parseInt(size) || 50;
		let chunks = [];
		for (let i = 0; i < RawCSVData.data.length; i += chunkSize) {
			const chunk = RawCSVData.data.slice(i, i + chunkSize);
			chunks.push(chunk);
		}

		SetParsedChunks(chunks);
		SetCurrentChunk(chunks[0]);
		SetCurrentChunkId(0);
	}

	useEffect(() => {
		if (!props.file) {
			return;
		}
		const csv = new Buffer(props.file, "base64").toString("binary");
		let parsedCsv = csvParse.parse(csv, { header: true });
		SetRawCSVData(parsedCsv);
		let chunks = [];
		for (let i = 0; i < parsedCsv.data.length; i += 50) {
			const chunk = parsedCsv.data.slice(i, i + 50);
			chunks.push(chunk);
		}

		SetParsedChunks(chunks);
		SetCurrentChunk(chunks[0]);
		SetCurrentChunkId(0);
	}, [props.file]);

	async function DownloadBox() {
		SetIsFileDownloading(true);
		await saveFile(
			`api/box/${props.data.box._id}/download`,
			`${props.data.box.name}.csv`
		);
		SetIsFileDownloading(false);
	}

	async function handleFileChangeClick() {
		fileUploadRef.current.click();
	}

	function IsBoxOwner() {
		let isOwner = false;
		if (
			(Cookies.get("USER_ID") &&
				props.data?.box?.owner?.id &&
				props.data?.box?.owner?.id === Cookies.get("USER_ID")) ||
			Cookies.get("SUPER_USER") === "true"
		) {
			isOwner = true;
		}
		return isOwner;
	}

	async function handleDeleteRequest() {
		if (!IsBoxOwner) {
			return;
		}

		SetIsFileDeleting(true);

		const request = await fetch(`api/box/${props?.data?.box?._id}/delete`, {
			method: "POST",
		});

		if (!request || request.ok === false) {
			SetIsFileDeleting(false);
			return;
		}

		SetIsFileDeleting(false);
		navigate("/", { replace: true });
	}

	async function handleFileChange(e) {
		let value = e.target.files && e.target.files[0];
		e.target.value = null;
		if (!IsBoxOwner) {
			return;
		}
		if (!value) {
			return;
		}
		if (value) {
			if (value.size >= 2.5e6) {
				return false;
			}
			if (value.type !== "text/csv" && value.type !== "application/csv") {
				return false;
			}
		}

		SetIsFileUploading(true);

		const requestBody = new FormData();
		requestBody.append("boxFile", value);

		const request = await fetch(`api/box/${props?.data?.box?._id}/upload`, {
			method: "POST",
			body: requestBody,
		});

		if (!request || request.ok === false) {
			SetIsFileUploading(false);
			return;
		}

		await request.json();

		SetIsFileUploading(false);
		window.location.reload();
	}

	return (
		<Container
			fluid
			style={{ margin: "0rem", marginTop: "-2rem", padding: "2rem" }}>
			{!props.file && !ParsedChunks ? (
				<Segment placeholder>
					<Header icon>
						<Icon name="file outline" />
						This box doesn't have any content
					</Header>
					<input
						style={{ display: "none" }}
						ref={fileUploadRef}
						type="file"
						accept=".csv, application/csv, text/csv"
						onChange={handleFileChange}></input>
					<Button
						primary
						disabled={!IsBoxOwner() || IsFileUploading}
						loading={IsFileUploading}
						onClick={handleFileChangeClick}>
						Upload Document
					</Button>
				</Segment>
			) : (
				<Segment>
					<Segment basic style={{ paddingBottom: 0 }}>
						<Button
							primary
							loading={IsFileDownloading}
							onClick={() => DownloadBox()}>
							<Icon name="download"></Icon>
							Unbox Document
						</Button>
						<Button
							negative
							disabled={IsFileDeleting || !IsBoxOwner()}
							loading={IsFileDeleting}
							onClick={() => handleDeleteRequest()}>
							<Icon name="delete"></Icon>
							Delete Document
						</Button>
						<input
							style={{ display: "none" }}
							ref={fileUploadRef}
							type="file"
							accept=".csv, application/csv, text/csv"
							onChange={handleFileChange}></input>
						<Button
							secondary
							disabled={!IsBoxOwner() || IsFileUploading}
							loading={IsFileUploading}
							onClick={() => handleFileChangeClick()}>
							<Icon name="file"></Icon>
							Change Document
						</Button>
						<Input
							icon="list"
							iconPosition="left"
							placeholder="Amount of rows"
							value={ChunkSize}
							onChange={(_, v) => {
								let value = v.value.replace(/\D/g, "");
								if (parseInt(value)) {
									value = parseInt(value);
									if (value > 250) {
										value = 250;
									}
								}
								SetChunkSize(value);
								ChunkParsedData(value);
							}}></Input>
					</Segment>
					<Segment basic style={{ paddingTop: 0 }}>
						<Table celled fixed>
							<Table.Header>
								<Table.Row>
									{RawCSVData?.meta?.fields?.map((v, index) => (
										<Table.HeaderCell key={index}>
											{v}
										</Table.HeaderCell>
									))}
								</Table.Row>
							</Table.Header>
							<Table.Body>
								{CurrentChunk?.map((v, k) => {
									return (
										<Table.Row key={k}>
											{Object.keys(v).map((_, index) => (
												<Table.Cell key={index}>
													{Object.values(v)[index]}
												</Table.Cell>
											))}
										</Table.Row>
									);
								})}
							</Table.Body>

							<Table.Footer>
								<Table.Row>
									{CurrentChunkId !== undefined &&
									ParsedChunks !== undefined ? (
										<Table.HeaderCell
											colSpan={RawCSVData?.meta?.fields?.length}
											textAlign="right">
											<Pagination
												activePage={CurrentChunkId + 1}
												onPageChange={(_, data) => {
													SetCurrentChunkId(data.activePage - 1);
													SetCurrentChunk(
														ParsedChunks[data.activePage - 1]
													);
												}}
												firstItem={null}
												lastItem={null}
												prevItem={{
													content: <Icon name="chevron left" />,
													icon: true,
												}}
												nextItem={{
													content: <Icon name="chevron right" />,
													icon: true,
												}}
												totalPages={ParsedChunks?.length}
											/>
										</Table.HeaderCell>
									) : (
										<></>
									)}
								</Table.Row>
							</Table.Footer>
						</Table>
					</Segment>
				</Segment>
			)}
		</Container>
	);
}
