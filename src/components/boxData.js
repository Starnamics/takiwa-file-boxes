import { Container, Header, Segment, Button } from "semantic-ui-react";
import EditModal from "./editModal";
import ShareModal from "./shareModal";
import { useState } from "react";
import Cookies from "js-cookie";

export default function BoxData(props) {
	const [IsEditModalOpen, SetEditModalOpen] = useState(false);
	const [IsShareModalOpen, SetShareModalOpen] = useState(false);

	function IsBoxOwner() {
		let isOwner = false;
		if (
			(Cookies.get("USER_ID") &&
				props.data?.box?.owner?.id &&
				props.data?.box?.owner?.id === Cookies.get("USER_ID")) ||
			Cookies.get("SUPER_USER") === "true"
		) {
			isOwner = true;
		}
		return isOwner;
	}

	return (
		<div>
			{props.data && props.data.success ? (
				<Container fluid textAlign="left" style={{ padding: "2rem" }}>
					<Segment>
						<Header size="large">{props?.data?.box?.name}</Header>
						<Header
							color="grey"
							size="medium"
							style={{ marginTop: "-.5rem" }}>
							<Header.Content>
								{props.data?.box?.owner?.username
									? props.data?.box?.owner?.username
									: "an anonymous user"}
							</Header.Content>
							<Button
								onClick={() => SetEditModalOpen(true)}
								disabled={!IsBoxOwner()}
								style={{
									position: "absolute",
									right: 0,
									top: 0,
									marginTop: "1rem",
									marginRight: "1.5rem",
								}}>
								Edit
							</Button>
							<Button
								onClick={() => SetShareModalOpen(true)}
								disabled={!IsBoxOwner()}
								style={{
									position: "absolute",
									right: 0,
									top: 0,
									marginTop: "1rem",
									marginRight: "7rem",
								}}>
								Share
							</Button>
						</Header>
						{props?.data?.box?.description ? (
							<p style={{ color: "grey" }}>
								{props?.data?.box?.description}
							</p>
						) : (
							<></>
						)}
					</Segment>
				</Container>
			) : (
				<Container fluid textAlign="left" style={{ padding: "2rem" }}>
					<Segment>
						<Header size="large">Invalid Box</Header>
					</Segment>
				</Container>
			)}
			<EditModal
				IsOpen={IsEditModalOpen}
				SetOpen={SetEditModalOpen}
				data={props?.data}></EditModal>
			<ShareModal
				IsOpen={IsShareModalOpen}
				SetOpen={SetShareModalOpen}
				data={props?.data}></ShareModal>
		</div>
	);
}
