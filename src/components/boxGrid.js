import {
	Grid,
	Segment,
	Header,
	Label,
	Icon,
	Button,
	Container,
	Loader,
	Dimmer,
	Input,
} from "semantic-ui-react";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function BoxGrid(props) {
	const [Boxes, SetBoxes] = useState();
	const [VisibleBoxes, SetVisibleBoxes] = useState();
	let navigate = useNavigate();

	useEffect(() => {
		if (!props?.type) {
			return;
		}
		fetch(`api/boxes?type=${props?.type}`).then((v) => {
			v.json().then((body) => {
				body.boxes.reverse();
				SetBoxes(body.boxes);
				SetVisibleBoxes(body.boxes);
			});
		});
	}, [props?.type]);

	return (
		<div>
			<Segment
				basic
				style={{
					margin: "2rem",
					marginBottom: "0rem",
					paddingBottom: "0rem",
				}}>
				<Input
					fluid
					action="Search"
					placeholder="Search for a box"
					icon="search"
					iconPosition="left"
					onChange={(_, v) => {
						const searchQuery = v.value;
						if (searchQuery.length > 0) {
							const filteredArray = Boxes.filter((data) =>
								data.name
									.toLowerCase()
									.includes(searchQuery.toLowerCase())
							);
							SetVisibleBoxes(filteredArray);
						} else {
							SetVisibleBoxes(Boxes);
						}
					}}
				/>
			</Segment>
			<Grid columns={6} padded={"horizontally"} style={{ padding: "2rem" }}>
				{VisibleBoxes && VisibleBoxes.length >= 0 ? (
					VisibleBoxes.map((data) => {
						return (
							<Grid.Column key={data._id}>
								<Segment
									raised
									color={data.visibility === 1 ? "blue" : "grey"}>
									<Header style={{ marginBottom: 0 }}>
										{data.name}
									</Header>
									<Header
										size="small"
										color="grey"
										style={{ marginTop: ".15rem" }}>
										Owned by{" "}
										{data.owner
											? data.owner?.username
											: "an anoymous user"}
									</Header>
									<Button
										primary
										onClick={() => {
											navigate(`/${data._id}`);
										}}>
										<Icon name="folder open"></Icon>
										View Box Contents
									</Button>
									<Container style={{ marginTop: "1rem" }}>
										<Label size="small">
											<Icon name="calendar" />{" "}
											{data.creationDate
												? new Intl.DateTimeFormat("en-US").format(
														data.creationDate
												  )
												: "N/A"}
										</Label>
										<Label size="small">
											<Icon name="chart line" />{" "}
											{data.views
												? Intl.NumberFormat("en-US", {
														notation: "compact",
														maximumFractionDigits: 1,
												  }).format(data.views)
												: "0"}
										</Label>
										<Label size="small">
											<Icon name="download" />{" "}
											{data.downloads
												? Intl.NumberFormat("en-US", {
														notation: "compact",
														maximumFractionDigits: 1,
												  }).format(data.downloads)
												: "0"}
										</Label>
									</Container>
								</Segment>
							</Grid.Column>
						);
					})
				) : (
					<Dimmer active>
						<Loader indeterminate>Loading Boxes</Loader>
					</Dimmer>
				)}
			</Grid>
		</div>
	);
}
