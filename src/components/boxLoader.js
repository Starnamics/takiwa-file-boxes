import BoxData from "./boxData";
import BoxFile from "./boxFile";
import { useEffect, useState } from "react";
import { Dimmer, Loader, Segment } from "semantic-ui-react";

export default function BoxLoader(props) {
	const [isFileLoading, setFileLoading] = useState(true);
	const [file, setFile] = useState();

	useEffect(() => {
		if (!props?.data?.box?._id) {
			return;
		}
		fetch(`api/box/${props?.data?.box?._id}/file`)
			.then((v) => {
				v.json().then((body) => {
					setFile(body?.file);
					setFileLoading(false);
				});
			})
			.catch(() => {
				setFileLoading(false);
			});
	}, [props?.data?.box?._id]);

	return (
		<>
			<BoxData data={props.data}></BoxData>
			{isFileLoading ? (
				<Segment
					style={{
						margin: "2rem",
						padding: "4rem",
						marginTop: "0rem",
						marginBottom: "0rem",
					}}>
					<Dimmer active inverted>
						<Loader inverted>Fetching Box Content</Loader>
					</Dimmer>
				</Segment>
			) : (
				<BoxFile file={file} data={props.data}></BoxFile>
			)}
		</>
	);
}
